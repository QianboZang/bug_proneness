import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.dummy import DummyClassifier
from sklearn.model_selection import cross_validate
import train_classifiers as tc
import matplotlib.pyplot as plt
import scipy
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler


def best_dt():
    # 1. Decision Tree
    model = DecisionTreeClassifier(criterion='entropy', max_depth=4, max_features=None)

    return model


def best_nb():
    # 2. Naive Bayes
    model = GaussianNB(var_smoothing=1e-10)

    return model


def best_svm():
    # 3. Support Vector Machine
    model = SVC(C=10)

    return model


def best_mlp():
    # 4. Multi-Layer Perceptron
    model = MLPClassifier(max_iter=2000, learning_rate_init=0.01)

    return model


def best_rf():
    # 5. Random Forest
    model = RandomForestClassifier(criterion='entropy', n_estimators=400, max_features=None)

    return model


def dummy():
    # 6. biased
    model = DummyClassifier(strategy='constant', constant=1)

    return model


def one_cv(model, X, Y, df):
    cv_results = cross_validate(
        model, X, Y,
        scoring={'f1': 'f1', 'precision': 'precision', 'recall': 'recall'}, cv=KFold(5)
    )
    f1 = cv_results['test_f1']
    p = cv_results['test_precision']
    r = cv_results['test_recall']
    for i in range(0, 5):
        df = df.append({'precision': p[i], 'recall': r[i], 'F1': f1[i]}, ignore_index=True)

    return df


def plot_p(df1, df2, df3, df4, df5, df6):
    dt_p = df1['precision'].values
    nb_p = df2['precision'].values
    svm_p = df3['precision'].values
    mlp_p = df4['precision'].values
    rf_p = df5['precision'].values
    bc_p = df6['precision'].values

    df_p = pd.DataFrame(columns=['DT', 'NB_recall', 'SVM', 'MLP', 'RF', 'bias'])
    df_p['DT'] = dt_p
    df_p['NB'] = nb_p
    df_p['SVM'] = svm_p
    df_p['MLP'] = mlp_p
    df_p['RF'] = rf_p
    df_p['bias'] = bc_p
    df_p.boxplot()
    plt.title("Precision")
    plt.show()


def plot_r(df1, df2, df3, df4, df5, df6):
    dt_r = df1['recall'].values
    nb_r = df2['recall'].values
    svm_r = df3['recall'].values
    mlp_r = df4['recall'].values
    rf_r = df5['recall'].values
    bc_r = df6['recall'].values

    df_p = pd.DataFrame(columns=['DT', 'NB_recall', 'SVM', 'MLP', 'RF', 'bias'])
    df_p['DT'] = dt_r
    df_p['NB'] = nb_r
    df_p['SVM'] = svm_r
    df_p['MLP'] = mlp_r
    df_p['RF'] = rf_r
    df_p['bias'] = bc_r
    df_p.boxplot()
    plt.title("Recall")
    plt.show()


def plot_F1(df1, df2, df3, df4, df5, df6):
    dt_f = df1['F1'].values
    nb_f = df2['F1'].values
    svm_f = df3['F1'].values
    mlp_f = df4['F1'].values
    rf_f = df5['F1'].values
    bc_f = df6['F1'].values

    df_p = pd.DataFrame(columns=['DT', 'NB_recall', 'SVM', 'MLP', 'RF', 'bias'])
    df_p['DT'] = dt_f
    df_p['NB'] = nb_f
    df_p['SVM'] = svm_f
    df_p['MLP'] = mlp_f
    df_p['RF'] = rf_f
    df_p['bias'] = bc_f
    df_p.boxplot()
    plt.title("F1")
    plt.show()


def get_models():
    model_dt = best_dt()
    model_nb = best_nb()
    model_svm = best_svm()
    model_mlp = best_mlp()
    model_rf = best_rf()
    model_bc = dummy()

    return model_dt, model_nb, model_svm, model_mlp, model_rf, model_bc


def plot_all(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc):
    plot_p(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)
    plot_r(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)
    plot_F1(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)


def compare_p(df1, df2):
    print(scipy.stats.wilcoxon(df1["precision"], df2["precision"]).pvalue)


def show_p(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc):
    print("mean precision of Decision Tree:", df_dt["precision"].mean())
    print("mean precision of Naive Bayes:", df_nb["precision"].mean())
    print("mean precision of Support Vector Machine:", df_svm["precision"].mean())
    print("mean precision of Multi-layer Perceptron:", df_mlp["precision"].mean())
    print("mean precision of Random Forest:", df_rf["precision"].mean())
    print("mean precision of Biased Classifier:", df_bc["precision"].mean())

    print('01 P-value of Decision Tree and Naive Bayes:')
    compare_p(df_dt, df_nb)
    print('02 P-value of Decision Tree and SVM:')
    compare_p(df_dt, df_svm)
    print('03 P-value of Decision Tree and MLP:')
    compare_p(df_dt, df_mlp)
    print('04 P-value of Decision Tree and Random Forest:')
    compare_p(df_dt, df_rf)
    print('05: P-value of Decision Tree and Biased Classifier:')
    compare_p(df_dt, df_bc)

    print('06: P-value of Naive Bayes and SVM:')
    compare_p(df_nb, df_svm)
    print('07: P-value of Naive Bayes and Naive MLP:')
    compare_p(df_nb, df_mlp)
    print('08: P-value of Naive Bayes and Random Forest:')
    compare_p(df_nb, df_rf)
    print('09: P-value of Naive Bayes and Biased Classifier:')
    compare_p(df_nb, df_bc)

    print('10: P-value of SVM and MLP:')
    compare_p(df_svm, df_mlp)
    print('11: P-value of SVM and Random Forest:')
    compare_p(df_svm, df_rf)
    print('12: P-value of SVM and Biased Classifier:')
    compare_p(df_svm, df_bc)

    print('13: P-value of MLP and Random Forest:')
    compare_p(df_mlp, df_rf)
    print('14: P-value of MLP and Biased Classifier:')
    compare_p(df_mlp, df_bc)

    print('15: P-value of Random Forest and Biased Classifier:')
    compare_p(df_rf, df_bc)


def compare_r(df1, df2):
    print(scipy.stats.wilcoxon(df1["recall"], df2["recall"]).pvalue)


def show_r(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc):
    print("mean recall of Decision Tree:", df_dt["recall"].mean())
    print("mean recall of Naive Bayes:", df_nb["recall"].mean())
    print("mean recall of Support Vector Machine:", df_svm["recall"].mean())
    print("mean recall of Multi-layer Perceptron:", df_mlp["recall"].mean())
    print("mean recall of Random Forest:", df_rf["recall"].mean())
    print("mean recall of Biased Classifier:", df_bc["recall"].mean())

    print('01 P-value of Decision Tree and Naive Bayes:')
    compare_r(df_dt, df_nb)
    print('02 P-value of Decision Tree and SVM:')
    compare_r(df_dt, df_svm)
    print('03 P-value of Decision Tree and MLP:')
    compare_r(df_dt, df_mlp)
    print('04 P-value of Decision Tree and Random Forest:')
    compare_r(df_dt, df_rf)
    print('05: P-value of Decision Tree and Biased Classifier:')
    compare_r(df_dt, df_bc)

    print('06: P-value of Naive Bayes and SVM:')
    compare_r(df_nb, df_svm)
    print('07: P-value of Naive Bayes and Naive MLP:')
    compare_r(df_nb, df_mlp)
    print('08: P-value of Naive Bayes and Random Forest:')
    compare_r(df_nb, df_rf)
    print('09: P-value of Naive Bayes and Biased Classifier:')
    compare_r(df_nb, df_bc)

    print('10: P-value of SVM and MLP:')
    compare_r(df_svm, df_mlp)
    print('11: P-value of SVM and Random Forest:')
    compare_r(df_svm, df_rf)
    print('12: P-value of SVM and Biased Classifier:')
    compare_r(df_svm, df_bc)

    print('13: P-value of MLP and Random Forest:')
    compare_r(df_mlp, df_rf)
    print('14: P-value of MLP and Biased Classifier:')
    compare_r(df_mlp, df_bc)

    print('15: P-value of Random Forest and Biased Classifier:')
    compare_r(df_rf, df_bc)


def compare_f(df1, df2):
    print(scipy.stats.wilcoxon(df1["F1"], df2["F1"]).pvalue)


def show_f(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc):
    print("mean f1 of Decision Tree:", df_dt["F1"].mean())
    print("mean f1 of Naive Bayes:", df_nb["F1"].mean())
    print("mean f1 of Support Vector Machine:", df_svm["F1"].mean())
    print("mean f1 of Multi-layer Perceptron:", df_mlp["F1"].mean())
    print("mean f1 of Random Forest:", df_rf["F1"].mean())
    print("mean f1 of Biased Classifier:", df_bc["F1"].mean())

    print('01 P-value of Decision Tree and Naive Bayes:')
    compare_f(df_dt, df_nb)
    print('02 P-value of Decision Tree and SVM:')
    compare_f(df_dt, df_svm)
    print('03 P-value of Decision Tree and MLP:')
    compare_f(df_dt, df_mlp)
    print('04 P-value of Decision Tree and Random Forest:')
    compare_f(df_dt, df_rf)
    print('05: P-value of Decision Tree and Biased Classifier:')
    compare_f(df_dt, df_bc)

    print('06: P-value of Naive Bayes and SVM:')
    compare_f(df_nb, df_svm)
    print('07: P-value of Naive Bayes and Naive MLP:')
    compare_f(df_nb, df_mlp)
    print('08: P-value of Naive Bayes and Random Forest:')
    compare_f(df_nb, df_rf)
    print('09: P-value of Naive Bayes and Biased Classifier:')
    compare_f(df_nb, df_bc)

    print('10: P-value of SVM and MLP:')
    compare_f(df_svm, df_mlp)
    print('11: P-value of SVM and Random Forest:')
    compare_f(df_svm, df_rf)
    print('12: P-value of SVM and Biased Classifier:')
    compare_f(df_svm, df_bc)

    print('13: P-value of MLP and Random Forest:')
    compare_f(df_mlp, df_rf)
    print('14: P-value of MLP and Biased Classifier:')
    compare_f(df_mlp, df_bc)

    print('15: P-value of Random Forest and Biased Classifier:')
    compare_f(df_rf, df_bc)


def main():
    X, Y = tc.load_data()
    ss = StandardScaler()
    X_std = ss.fit_transform(X)

    model_dt, model_nb, model_svm, model_mlp, model_rf, model_bc = get_models()
    df_dt = pd.DataFrame(columns=['precision', 'recall', 'F1'])
    df_nb = pd.DataFrame(columns=['precision', 'recall', 'F1'])
    df_svm = pd.DataFrame(columns=['precision', 'recall', 'F1'])
    df_mlp = pd.DataFrame(columns=['precision', 'recall', 'F1'])
    df_rf = pd.DataFrame(columns=['precision', 'recall', 'F1'])
    df_bc = pd.DataFrame(columns=['precision', 'recall', 'F1'])
    for t in range(0, 20):
        df_dt = one_cv(model_dt, X_std, Y, df_dt)
        df_nb = one_cv(model_nb, X_std, Y, df_nb)
        df_svm = one_cv(model_svm, X_std, Y, df_svm)
        df_mlp = one_cv(model_mlp, X_std, Y, df_mlp)
        df_rf = one_cv(model_rf, X_std, Y, df_rf)
        df_bc = one_cv(model_bc, X_std, Y, df_bc)

    # plot_all(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)
    show_p(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)
    # show_r(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)
    # show_f(df_dt, df_nb, df_svm, df_mlp, df_rf, df_bc)


if __name__ == "__main__":
    main()
