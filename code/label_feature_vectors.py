import pandas as pd
import os
import javalang
import numpy as np


def get_files(input_path):
    list_of_files = []
    for (path, dirs, files) in os.walk(input_path):
        # reference:
        # https://thispointer.com/python-how-to-get-list-of-files-in-directory-and-sub-directories/
        list_of_files += [os.path.join(path, file) for file in files]

    return list_of_files


def get_bug_lists(list_of_files):
    bug_lists = []
    for file_path in list_of_files:
        with open(file_path, "r") as f:
            contents = f.readlines()
            for content in contents:
                bug_name = content.split('.')[-1]
                bug_name = bug_name.split('\n')[0]
                bug_lists.append(bug_name)

    return bug_lists


def load_data():
    data = pd.read_csv("./feature_vector_file.csv").iloc[:, 1:].values

    return data


def create_labels(data, bug_lists):
    count = 0
    buggy = np.zeros(data.shape[0], dtype=int)
    class_lists = data[:, 0]
    for i, class_name in enumerate(class_lists):
        if class_name in bug_lists:
            count += 1
            buggy[i] = 1
    print("The number of bug classes:", count)
    return buggy


def main():
    input_path = "./resources/modified_classes/"
    list_of_files = get_files(input_path)
    bug_lists = get_bug_lists(list_of_files)
    # print(len(bug_lists))
    data = load_data()
    buggy = create_labels(data, bug_lists)
    new_data = np.c_[data, buggy]
    new_data = pd.DataFrame(
        new_data,
        columns=['Class', 'MTH', 'FLD', 'RFC', 'INT', 'SZ', 'CPX', 'EX', 'RET', 'BCM', 'NML', 'WRD', 'DCM', 'buggy']
    )
    # new_data.to_csv("label_feature_vector_file.csv")
    print(new_data)


if __name__ == "__main__":
    main()
