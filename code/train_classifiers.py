import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import precision_recall_fscore_support
from sklearn.preprocessing import StandardScaler


def load_data():
    path = "label_feature_vector_file.csv"
    data = pd.read_csv(path).iloc[:, 1:]
    X = data.iloc[:, 1: -1].values
    Y = data.iloc[:, -1].values

    return X, Y


def decision_tree():
    # 1. Decision Tree
    parameters = {
        'criterion': ['gini', 'entropy'],
        'max_features': [None, 'log2', 'sqrt'],
        'max_depth': range(3, 15)
    }
    model = DecisionTreeClassifier()
    my_model = {
        "model": model,
        "parameters": parameters
    }

    return my_model


def naive_bayes():
    # 2. Naive Bayes
    parameters = {
        "var_smoothing": [1e-10, 1e-9, 1e-8, 1e-7]
    }
    model = GaussianNB()
    my_model = {
        "model": model,
        "parameters": parameters
    }

    return my_model


def support_vector_machine():
    # 3. Support Vector Machine
    parameters = {
        "C": [0.0001, 0.001, 0.01, 0.1, 1.0, 10]
    }
    model = SVC()
    my_model = {
        "model": model,
        "parameters": parameters
    }

    return my_model


def mlp_classifier():
    # 4. Multi-Layer Perceptron
    parameters = {
        'max_iter': [2000, 2500, 3000],
        'learning_rate_init': [0.01, 0.005, 0.001]
    }
    model = MLPClassifier()
    my_model = {
        "model": model,
        "parameters": parameters
    }

    return my_model


def random_forest():
    # 5. Random Forest
    parameters = {
        'n_estimators': [200, 300, 400],
        'max_features': [None, 'sqrt', 'log2'],
        'criterion': ['gini', 'entropy']
    }
    model = RandomForestClassifier()
    my_model = {
        "model": model,
        "parameters": parameters
    }

    return my_model


def optimisation(my_model, x_train, y_train):
    model = my_model["model"]
    parameters = my_model["parameters"]
    grid = GridSearchCV(estimator=model, param_grid=parameters)
    grid.fit(x_train, y_train)

    return grid


def display_best_model(grid):
    # print(grid.cv_results_)
    print(grid.best_estimator_)
    print(grid.best_score_)


def prediction(grid, x_test, y_test):
    ypre = grid.predict(x_test)
    print(precision_recall_fscore_support(y_test, ypre, average='binary'))


def main():
    X, Y = load_data()
    ss = StandardScaler()
    X_std = ss.fit_transform(X)
    x_train, x_test, y_train, y_test = train_test_split(X_std, Y, test_size=0.2)

    d_tree_model = decision_tree()
    # n_bayes_model = naive_bayes()
    # s_machine_model = support_vector_machine()
    # m_perceptron_model = mlp_classifier()
    # r_forest_model = random_forest()

    d_tree = optimisation(d_tree_model, x_train, y_train)
    # n_bayes = optimisation(n_bayes_model, x_train, y_train)
    # s_machine = optimisation(s_machine_model, x_train, y_train)
    # m_perceptron = optimisation(m_perceptron_model, x_train, y_train)
    # r_forest = optimisation(r_forest_model, x_train, y_train)

    display_best_model(d_tree)
    # display_best_model(n_bayes)
    # display_best_model(s_machine)
    # display_best_model(m_perceptron)
    # display_best_model(r_forest)

    prediction(d_tree, x_test, y_test)
    # prediction(n_bayes, x_test, y_test)
    # prediction(s_machine, x_test, y_test)
    # prediction(m_perceptron, x_test, y_test)
    # prediction(r_forest, x_test, y_test)


if __name__ == "__main__":
    main()
