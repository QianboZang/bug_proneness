import pandas as pd
import os
import javalang
import re


def get_files(input_path):
    list_of_files = []
    for (path, dirs, files) in os.walk(input_path):
        for file in files:
            file_name, check_java = os.path.splitext(file)
            if check_java == ".java":
                list_of_files.append(os.path.join(path, file))

    return list_of_files


def find_trees(list_of_files):
    tree_lists = []
    for path in list_of_files:
        with open(path, 'r') as f:
            content = f.read()
            tree = javalang.parse.parse(content)
            tree_lists.append(tree)

    return tree_lists


def access_classes(tree_lists):
    class_name = []
    for tree in tree_lists:
        for _, node in tree:
            if type(node) == javalang.tree.ClassDeclaration and node.name == tree.types[0].name:
                class_name.append(node.name)
    class_name = set(class_name)

    return list(class_name)


def MTH_FLD(node, df):
    # Methods
    df.loc[df.Class == node.name, 'MTH'] = len(node.methods)
    # Fields
    df.loc[df.Class == node.name, 'FLD'] = len(node.fields)


def RFC(node, df):
    pub_met = 0
    cal_met = 0
    # Public methods
    for method in node.methods:
        if method.modifiers == {'public'}:
            pub_met += 1
    df.loc[df.Class == node.name, 'RFC'] = pub_met
    # Called methods
    for _, sub_node in node:
        if type(sub_node) == javalang.tree.MethodInvocation:
            cal_met += 1
    df.loc[df.Class == node.name, 'RFC'] = cal_met


def INT(node, df):
    # Implemented interfaces
    if node.implements is not None:
        df.loc[df.Class == node.name, 'INT'] = len(node.implements)


def SZ(node, df):
    state = 0
    for method in node.methods:
        # Statements
        for _, sub_meth in method:
            if type(sub_meth).__base__ == javalang.tree.Statement:
                state += 1
    df.loc[df.Class == node.name, 'SZ'] = state


def CPX(node, df):
    cond = 0
    loop = 0
    for method in node.methods:
        # CONDITIONAL
        for _, sub_meth in method:
            if type(sub_meth) == javalang.tree.IfStatement:
                cond += 1
        # LOOP statements
        for _, sub_meth in method:
            if type(sub_meth) == javalang.tree.WhileStatement or type(sub_meth) == javalang.tree.ForStatement or type(sub_meth) == javalang.tree.DoStatement:
                loop += 1
    df.loc[df.Class == node.name, 'CPX'] = cond + loop


def EX(node, df):
    exc = 0
    for method in node.methods:
        # Exceptions in throws clause
        for _, sub_meth in method:
            if type(sub_meth) == javalang.tree.ThrowStatement:
                exc += 1
    df.loc[df.Class == node.name, 'EX'] = exc


def RET(node, df):
    ret = 0
    for method in node.methods:
        # Return points
        for _, sub_meth in method:
            if type(sub_meth) == javalang.tree.ReturnStatement:
                ret += 1
    df.loc[df.Class == node.name, 'RET'] = ret


def BCM(node, df):
    # Block comments
    doc = 0
    if node.documentation is not None:
        doc += 1
    for meth in node.methods:
        if meth.documentation is not None:
            doc += 1

    df.loc[df.Class == node.name, 'BCM'] = doc


def NML(node, df):
    # Average length of method names
    avg = 0
    total = 0
    for method in node.methods:
        total += len(method.name)
    if len(node.methods) != 0:
        avg = total / len(node.methods)
    df.loc[df.Class == node.name, 'NML'] = round(avg, 3)


def WRD(node, df):
    # Words (longest alphanumeric substrings) in block comments
    long = 0
    for meth in node.methods:
        if meth.documentation is not None:
            if long < len(re.findall("\w+", meth.documentation)):
                long = len(re.findall("\w+", meth.documentation))

    df.loc[df.Class == node.name, 'WRD'] = long


def DCM(node, df):
    # Words in comments / Statements
    words = 0
    if node.documentation is not None:
        words = len(re.findall('\w+', node.documentation))
    state = df.loc[df.Class == node.name, 'SZ'].values
    if state == 0:
        df.loc[df.Class == node.name, 'DCM'] = 0
    else:
        df.loc[df.Class == node.name, 'DCM'] = words / state


def make_df(tree_lists, df, class_name):
    for tree in tree_lists:
        for _, node in tree:
            if type(node) == javalang.tree.ClassDeclaration and node.name in class_name:
                MTH_FLD(node, df)
                RFC(node, df)
                INT(node, df)
                SZ(node, df)
                CPX(node, df)
                EX(node, df)
                RET(node, df)
                BCM(node, df)
                NML(node, df)
                WRD(node, df)
                DCM(node, df)


def main():
    input_path = "./resources/defects4j-checkout-closure-1f/src/com/google/javascript/jscomp/"
    list_of_files = get_files(input_path)
    tree_lists = find_trees(list_of_files)
    class_name = access_classes(tree_lists)
    df = pd.DataFrame(columns=['Class', 'MTH', 'FLD', 'RFC', 'INT', 'SZ', 'CPX', 'EX', 'RET', 'BCM', 'NML', 'WRD', 'DCM'])
    df['Class'] = class_name
    df = df.fillna(0)
    make_df(tree_lists, df, class_name)
    print(df)
    pd.set_option('display.max_columns', None)
    print(df.describe())
    # df.to_csv("feature_vector_file.csv")


if __name__ == "__main__":
    main()
